﻿using System;
using System.IO;
using System.Xml.Linq;

namespace PrefabBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            var errorPath = "errors.txt";

            if (File.Exists(errorPath))
            {
                File.Delete(errorPath);
            }

            try
            {
                var output = new XDocument();
                var root = new XElement("game_data");
                output.Add(root);


                foreach (var file in Directory.EnumerateFiles(".", "*.csv"))
                {
                    try
                    {
                        Console.WriteLine("Reading " + file);

                        var prefabRoot = new XElement("city_prefab");
                        prefabRoot.SetAttributeValue("id", Path.GetFileNameWithoutExtension(file));
                        prefabRoot.SetAttributeValue("race", "?");
                        prefabRoot.SetAttributeValue("is_city_starter", "true");

                        var lines = File.ReadAllLines(file);

                        for(var y = 0; y < lines.Length; y++)
                        {
                            var tokens = lines[y].Split(',');

                            for(var x = 0; x < tokens.Length; x++)
                            {
                                if (!string.IsNullOrWhiteSpace(tokens[x]))
                                {
                                    var placement = new XElement("structure");
                                    prefabRoot.Add(placement);

                                    placement.SetAttributeValue("x", x);
                                    placement.SetAttributeValue("y", y);

                                    var split = tokens[x].Split(' ');

                                    if (!string.IsNullOrWhiteSpace(split[0]))
                                    {
                                        placement.SetAttributeValue("structure_id", split[0]);
                                    }

                                    if (split.Length > 1 && !string.IsNullOrWhiteSpace(split[1]))
                                    {
                                        placement.SetAttributeValue("recipie_id", split[1]);
                                    }
                                }
                            }
                        }

                        root.Add(prefabRoot);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.ToString()}: {ex.Message}");
                        using (var errorfile = File.Open(errorPath, FileMode.Append))
                        using (var writer = new StreamWriter(errorfile))
                        {
                            writer.WriteLine($"{DateTime.UtcNow}: {ex.ToString()} {ex.Message}");
                            writer.Close();
                        }
                    }
                }

                output.Save("ai_prefabs.xml");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.ToString()}: {ex.Message}");
                using (var errorfile = File.Open(errorPath, FileMode.Append))
                using (var writer = new StreamWriter(errorfile))
                {
                    writer.WriteLine($"{DateTime.UtcNow}: {ex.ToString()} {ex.Message}");
                    writer.Close();
                }
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
