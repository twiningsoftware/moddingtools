# README #

This repository holds tools to help with making mods for Bronze Age 3.

### Prefab Builder ###

The Prefab Builder is a tool designed to quickly generate city prefabs. It consumes .CSV files and processes them into a single city prefab gamedata file.

The name of the CSV file is the id of the prefab, the race you will have to fill in afterwards.

If a cell is empty it is ignored, if there is a single piece of text, that is the structure_id, if there is a space then it's interpreted as "structure_id recipie_id".

### Contact ###

[Bronze Age Community Discord](https://discord.gg/5eewDGT)